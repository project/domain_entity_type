# Domain Access Entity Type

## CONTENTS OF THIS FILE

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## Introduction

This module gives the possibility to manage access to an entity type by domain
(for example if we want to make a certain content type available only in one
domain)

This module doesn't manage content access, however if you want to manage
content access by domain, you could
use https://www.drupal.org/project/domain_entity.

## Requirements

This module requires Domain (domain) contrib module.

## Installation

- Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

## Configuration

The module has many submodules to manage access to each core entity type:

- Domain Content Type Access (det_node): this submodule is by default enabled
provides new allowed domain checkboxes on node type edit form to allow access
only for selected domains (if no domain is selected then all the content type
will be accessible on all domains), the module also expose a new permission
`bypass content type domain access check` to by access check for specific role.


Domain Access Entity Type module expose a global permission
`bypass all entity types domain access check` to bypass access check for all
entity type.

## Maintainers

- Akram Zairig (akram-zairig) - [akram-zairig](https://www.drupal.org/u/akram-zairig)
- Brahim Khouy (b.khouy) - [b.khouy](https://www.drupal.org/u/bkhouy)
