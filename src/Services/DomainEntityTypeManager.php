<?php

namespace Drupal\domain_entity_type\Services;

use Drupal\Core\Session\AccountInterface;

/**
 * Domain entity type manager service.
 */
class DomainEntityTypeManager implements DomainEntityTypeManagerInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Domain entity type manager constructor.
   */
  public function __construct(AccountInterface $currentUser) {
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritDoc}
   */
  public function bypassAccessCheck($entity_type = '') {
    if ($this->currentUser->hasPermission('bypass all entity types domain access check')) {
      return TRUE;
    }
    switch ($entity_type) {
      case 'node_type':
        return $this->currentUser->hasPermission('bypass content type domain access check');
      case 'default':
        return $this->currentUser->hasPermission('bypass all entity types domain access check');
    }
    return FALSE;
  }

}
