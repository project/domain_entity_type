<?php

namespace Drupal\domain_entity_type\Services;

/**
 * Domain entity type manager interface.
 */
interface DomainEntityTypeManagerInterface {

  /**
   * Check whither entity type access check should be bypassed.
   */
  public function bypassAccessCheck($entity_type = '');

}
