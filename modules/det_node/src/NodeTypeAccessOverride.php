<?php

namespace Drupal\det_node;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\domain_entity_type\Services\DomainEntityTypeManagerInterface;
use Drupal\node\NodeTypeAccessControlHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Node type access control handler override.
 */
class NodeTypeAccessOverride extends NodeTypeAccessControlHandler implements EntityHandlerInterface {

  /**
   * Domain negotiator service.
   *
   * @var DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * Domain entity type manager service.
   *
   * @var \Drupal\domain_entity_type\Services\DomainEntityTypeManagerInterface
   */
  protected $domainEntityTypeManager;

  /**
   * {@inheritDoc }
   */
  public function __construct(EntityTypeInterface $entity_type, DomainNegotiatorInterface $domainNegotiator, DomainEntityTypeManagerInterface $domainEntityTypeManager) {
    parent::__construct($entity_type);
    $this->domainNegotiator = $domainNegotiator;
    $this->domainEntityTypeManager = $domainEntityTypeManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('domain.negotiator'),
      $container->get('domain_entity_type.manager'),
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    $domains = $entity->getThirdPartySetting('det_node', 'domains', []);
    if (empty($domains) || $this->domainEntityTypeManager->bypassAccessCheck('node_type')) {
      return parent::checkAccess($entity, $operation, $account);
    }
    $current_domain = $this->domainNegotiator->getActiveId();
    if (!in_array($current_domain, $domains, TRUE)) {
      return AccessResult::forbidden('Node type access denied on current domain');
    }
    return parent::checkAccess($entity, $operation, $account);
  }

}
