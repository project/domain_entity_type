<?php

namespace Drupal\det_node\Controller;

use Drupal\node\Controller\NodeController;
use Drupal\node\Entity\NodeType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Node controller override.
 */
class NodeControllerOverride extends NodeController {

  /**
   * Domain negotiator service.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * Domain entity type manager service.
   *
   * @var \Drupal\domain_entity_type\Services\DomainEntityTypeManagerInterface
   */
  protected $domainEntityTypeManager;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->domainNegotiator = $container->get('domain.negotiator');
    $instance->domainEntityTypeManager = $container->get('domain_entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function addPage() {
    $build = parent::addPage();
    /** @var NodeType[] $node_types */
    $node_types = $build['#content'] ?? [];
    if (empty($node_types) || $this->domainEntityTypeManager->bypassAccessCheck('node_type')) {
      return $build;
    }
    foreach ($node_types as $id => $node_type) {
      $domains = $node_type->getThirdPartySetting('det_node', 'domains', []);
      if (empty($domains)) {
        continue;
      }
      $current_domain = $this->domainNegotiator->getActiveId();
      if (!in_array($current_domain, $domains, TRUE)) {
        unset($node_types[$id]);
      }
    }
    $build['#content'] = $node_types;
    return $build;
  }

}
