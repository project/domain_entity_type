<?php

namespace Drupal\det_node\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Node route subscriber.
 */
class NodeRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritDoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('node.add_page')) {
      $route->setDefault('_controller', '\Drupal\det_node\Controller\NodeControllerOverride::addPage');
    }
    if ($route = $collection->get('entity.node_type.delete_form')) {
      $route->setRequirement('_det_node_access_check', 'TRUE');
    }
    if ($route = $collection->get('entity.node_type.edit_form')) {
      $route->setRequirement('_det_node_access_check', 'TRUE');
    }
    if ($route = $collection->get('entity.node_type.entity_permissions_form')) {
      $route->setRequirement('_det_node_access_check', 'TRUE');
    }
  }

}
