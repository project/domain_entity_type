<?php

namespace Drupal\det_node;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\node\NodeTypeListBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Node type list builder override.
 */
class NodeTypeListBuilderOverride extends NodeTypeListBuilder {

  /**
   * Domain negotiator service.
   *
   * @var DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * Domain entity type manager service.
   *
   * @var \Drupal\domain_entity_type\Services\DomainEntityTypeManagerInterface
   */
  protected $domainEntityTypeManager;

  /**
   * {@inheritDoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->domainNegotiator = $container->get('domain.negotiator');
    $instance->domainEntityTypeManager = $container->get('domain_entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function buildRow(EntityInterface $entity) {
    $domains = $entity->getThirdPartySetting('det_node', 'domains', []);
    $current_domain = $this->domainNegotiator->getActiveId();
    if (!empty($domains) && !in_array($current_domain, $domains) && !$this->domainEntityTypeManager->bypassAccessCheck('node_type')) {
      return [];
    }
    return parent::buildRow($entity);
  }

}
