<?php

namespace Drupal\det_node\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\domain_entity_type\Services\DomainEntityTypeManagerInterface;
use Drupal\node\NodeTypeInterface;

/**
 * Node type routes domain access check.
 */
class NodeTypeDomainAccessCheck implements AccessInterface {

  /**
   * Domain negotiator service.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * Domain entity type manager service.
   *
   * @var \Drupal\domain_entity_type\Services\DomainEntityTypeManagerInterface
   */
  protected $domainEntityTypeManager;

  /**
   * {@inheritDoc}
   */
  public function __construct(DomainNegotiatorInterface $domainNegotiator, DomainEntityTypeManagerInterface $domainEntityTypeManager) {
    $this->domainNegotiator = $domainNegotiator;
    $this->domainEntityTypeManager = $domainEntityTypeManager;
  }

  /**
   * {@inheritDoc}
   */
  public function access(AccountInterface $account) {
    $params = \Drupal::routeMatch()->getParameters()->all();
    if (isset($params['node_type']) && $params['node_type'] instanceof NodeTypeInterface && !$this->domainEntityTypeManager->bypassAccessCheck('node_type')) {
      $domains = $params['node_type']->getThirdPartySetting('det_node', 'domains', []);
      $current_domain = $this->domainNegotiator->getActiveId();
      if (!empty($domains) && !in_array($current_domain, $domains)) {
        return AccessResult::forbidden('Node type access denied on current domain');
      }
    }
    return AccessResult::allowed();
  }

}
